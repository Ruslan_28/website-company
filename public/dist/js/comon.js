$(document).ready(function(){

    $('.slider-testimonials').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        nextArrow: $('.next'),
        prevArrow: $('.prev'),
    });

    $('.slider-cases').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        nextArrow: $('.next-cases'),
        prevArrow: $('.prev-cases'),
    });

    $('.slider-portfolio').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        adaptiveHeight: true,
        nextArrow: $('.next-portfolio'),
        prevArrow: $('.prev-portfolio'),
    });

    //animate slide
    $(".resume_item-about, .resume_item-web").animated("fadeInRight", "fadeOutRight");
    $(".resume_item-cases").animated("fadeInLeft", "fadeOutLeft");
    $(".resume_item-contact").animated("fadeInRight", "fadeOutRight");
    $(".icon-box, .image-abs, .about-box, .cases-box, .contact-form, .soft-text-box, .icons-abs").animated("fadeIn", "fadeOut");

    
    //tabs
    $('.tabs-stage div').hide();
    $('.tabs-stage div:first').show();
    $('.tabs-nav a:first').addClass('tab-active');

    // Change tab class and display content
    $('.tabs-nav a').on('click', function(event){
    event.preventDefault();
    $('.tabs-nav a').removeClass('tab-active');
    $(this).addClass('tab-active');
    $('.tabs-stage div').hide();
    $($(this).attr('href')).show();
});
    

});
